# assume a directory of reads called fastqs and a mlst database in a directory called mlst_db. A directory for results called ariba_mlst must also exist
call_mlst() {
  BASE_DIR=$PWD
  FILE=$1
  PREFIX=${FILE%_1.fastq.gz}
  PREFIX=${PREFIX#fastqs/*}
  ariba run \
  ${BASE_DIR}/mlst_db/ref_db \
  ${BASE_DIR}/fastqs/${PREFIX}_1.fastq.gz \
  ${BASE_DIR}/fastqs/${PREFIX}_2.fastq.gz \
  ${BASE_DIR}/ariba_mlst/${PREFIX} \
  > /dev/null 2>&1

  # add ST to file
  MLST_RESULT=$(tail -1  ${BASE_DIR}/ariba_mlst/${PREFIX}/mlst_report.tsv)
  echo -e "${PREFIX}\t${MLST_RESULT}" >> ${BASE_DIR}/full_STs.txt
}
export -f call_mlst
parallel --eta call_mlst ::: fastqs/*_1.fastq.gz