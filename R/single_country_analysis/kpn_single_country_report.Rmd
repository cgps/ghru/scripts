---
title: "Nigeria Kpn Analysis Retrospective 1"
author: "Anthony Underwood and Ayo Afolayan"
date: "12/08/2020"
output:
  html_document: 
    df_print: kable
    toc: yes
  pdf_document:
    df_print: kable
  word_document: default
---
```{r warning=FALSE, message=FALSE, echo=FALSE}
source("functions/single_country_functions.R")
# Read in data
kpn_metadata <- readr::read_csv("kpn_metadata.csv")
# remove ref
sample_metadata <- kpn_metadata %>% dplyr::rename(ST = ST__autocolour) %>%  tidyr::drop_na(Country)

# Get data about NCBI AMR gene
ncbi_amr_metadata <- get_ncbi_amr_metadata()

# Get point mutation metadata
point_mutation_metadata <- get_point_mutation_metadata("klebsiella")
```
### Summary of numbers from each sentinel site
Table 1: Describing total number of _Klebsiella pneumoniae_ samples submitted by Nigeria for Retrospective 1
```{r echo=FALSE, message=FALSE, warning=FALSE}
# Count numbers
samples_per_sentinel_site <-  count_samples_by_sentinel_site(sample_metadata, add_total = TRUE)

# print table
samples_per_sentinel_site
```

### Frequency of STs by Sentinel Site
Looking at the frequent STs in Nigeria for this period we see that ST307 is the dominant ST, but other high risk clones such as ST101 and ST147 are also observed.

Figure 1: All STs ordered by count

```{r echo=FALSE, message=FALSE, warning=FALSE}
st_counts <- count_sts(sample_metadata)
st_plot <- plot_sts(st_counts, order_by_count = TRUE)
print(st_plot)
```


Figure 2: Top 10 most frequent STs ordered by count

```{r echo=FALSE, message=FALSE, warning=FALSE}
most_frequent_sts <- count_most_frequent_sts(st_counts)
st_plot <- plot_sts(most_frequent_sts, order_by_count = TRUE)
print(st_plot)
```

Figure 3: Most frequent STs by Sentinel Site
```{r echo=FALSE, message=FALSE, warning=FALSE}
st_counts_by_sentinel_site <- count_sts_by_sentinel_site(sample_metadata)
most_frequent_st_counts_by_sentinel_site <- count_most_frequent_sts_per_sentinel_site(st_counts_by_sentinel_site, per_sentinel_site = 2)
plot_most_frequent_sentinel_site_sts(most_frequent_st_counts_by_sentinel_site)
```

### Examine AMR data
```{r echo=FALSE, message=FALSE, warning=FALSE, fig.height=7, fig.width=10}
# get long format data
acquired_and_point_amr_data <- pivot_metadata_to_long_amr(sample_metadata, ncbi_amr_metadata, point_mutation_metadata)
long_acquired_gene_amr_data <- acquired_and_point_amr_data[[1]]
acquired_gene_colnames <-  acquired_and_point_amr_data[[2]]
long_point_mutation_amr_data <- acquired_and_point_amr_data[[3]]
point_mutation_colnames <-  acquired_and_point_amr_data[[4]]

# key drug classes
key_drug_subclasses <- c("BETA-LACTAM", "CEPHALOSPORIN", "CARBAPENEM")

subclass_counts_by_sentinel_site <- count_AMR_subclasses_by_sentinel_site(long_acquired_gene_amr_data, samples_per_sentinel_site, key_drug_subclasses)
AMR_subclasses_plot <- plot_AMR_subclasses_by_sentinel_site(subclass_counts_by_sentinel_site)

gene_family_counts_by_sentinel_site <- count_gene_families_by_sentinel_site(long_acquired_gene_amr_data, subclass_counts_by_sentinel_site, key_drug_subclasses)
gene_family_dot_plot <- dot_plot_gene_family_counts_by_sentinel_site(gene_family_counts_by_sentinel_site)
plot_grid(AMR_subclasses_plot, gene_family_dot_plot,ncol =1, align="v", rel_heights = c(1, 3))
```

#### Gene alleles responsible for resistances to Cephalosporins and Carbapenems
It is important to look at the gene alleles responsible for resistance.

```{r echo=FALSE, message=FALSE, warning=FALSE}
drug_subclasses <- c('CEPHALOSPORIN', 'CARBAPENEM')
allele_counts_by_sentinel_site <- count_alleles_by_sentinel_site(long_acquired_gene_amr_data, gene_family_counts_by_sentinel_site, drug_subclasses)
allele_counts_plots <- plot_allele_counts_by_sentinel_site(allele_counts_by_sentinel_site, drug_subclasses)
```
```{r echo=FALSE, fig.height=15, fig.width=10, message=FALSE, warning=FALSE}
print(allele_counts_plots[[1]])
```
```{r echo=FALSE, fig.height=5, fig.width=10, message=FALSE, warning=FALSE}
print(allele_counts_plots[[2]])
```



