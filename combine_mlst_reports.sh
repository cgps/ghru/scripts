USAGE='combine_mlst_reports.sh -b <BASE MLST REPORT DIR>'

while getopts ":b:" option
do
    case "${option}" in
        b) BASE_DIR=${OPTARG};;
        \?) echo "Invalid option: -${OPTARG}" >&2
            exit 1;;
        :) echo "Option -${OPTARG} requires an argument" >&2
            exit 1;;
    esac
done

# Test for data dir
if [[ -z $BASE_DIR ]]
then
    echo "You must specify a base directory using -b"
    echo $USAGE
    exit
fi

SPECIES=$(basename ${BASE_DIR})

MLST_REPORT_FILES=($(ls ${BASE_DIR}/../../*/mlst_reports/${SPECIES}/combined_mlst_report.tsv))
for index in ${!MLST_REPORT_FILES[@]}; do
  MLST_REPORT_FILE=${MLST_REPORT_FILES[$index]}
  # add header line if first file
  if [[ $index -eq 0 ]]; then
    cat ${MLST_REPORT_FILE} > combined_mlst_report.tsv.tmp
  else
    awk 'NR>1 {print}' ${MLST_REPORT_FILE} >> combined_mlst_report.tsv.tmp
  fi
done
# archive old report
PREVIOUS_REPORT=$(ls combined_mlst_report.[0-9]*.tsv | sort | tail -1)
PREVIOUS_DATE=${PREVIOUS_REPORT#combined_mlst_report.*}
PREVIOUS_DATE=${PREVIOUS_DATE%*.tsv}
PREVIOUS_REPORT_ARCHIVED=combined_mlst_report.archived.${PREVIOUS_DATE}.tsv
mv ${PREVIOUS_REPORT} $PREVIOUS_REPORT_ARCHIVED

# make and sort new report
DATETIME=$(date '+%Y%m%d_%H%M')
head -n1 combined_mlst_report.tsv.tmp > combined_mlst_report.${DATETIME}.tsv
tail -n+2 combined_mlst_report.tsv.tmp | sort  >> combined_mlst_report.${DATETIME}.tsv
rm combined_mlst_report.tsv.tmp