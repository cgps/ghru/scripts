NEXTFLOW_PIPELINE_DIR=/data/nextflow_pipelines
DATA_DIR=/data/projects/ghru_retrospective_2018
SPECIES=<SPECIES>
DATE=<DATE>
RECOMBINATION=<RECOMBINATION>
REFERENCE=<REFERENCE>

nextflow run \
${NEXTFLOW_PIPELINE_DIR}/snp_phylogeny/snp_phylogeny.nf \
--input_dir ${DATA_DIR}/project_species_fastqs/${SPECIES} \
--fastq_pattern '*_{1,2}.fastq.gz' \
--output_dir ${DATA_DIR}/snp_phylogeny_output/${SPECIES}/${DATE} \
--reference ${DATA_DIR}/references/${REFERENCE} \
${RECOMBINATION} \
--tree \
--simple_output \
-w ${DATA_DIR}/snp_phylogeny_output/${SPECIES}/${DATE}/work \
-resume