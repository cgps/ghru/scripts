USAGE="soft_link_reads_and_assemblies_based_on_species.sh -b <BASE DATA DIR> -a <ANALYSIS DIRECTORY> -o <NAME OF ASSEMBLY OUTPUT DIR> -f <FASTQS DIR>
(optional add -w to overwrite files)"

# use greadlink on a mac
if hash greadlink 2>/dev/null; then
    shopt -s expand_aliases
    alias readlink=greadlink
fi

while getopts ":b:a:o:f:r:w:c" option
do
    case "${option}" in
        b) BASE_DIR=${OPTARG};;
        a) ANALYSIS_DIR=${OPTARG};;
        o) ASSEMBLY_OUTPUT_DIR=${OPTARG};;
        f) FASTQS_DIR=${OPTARG};;
        r) READ_SUFFIX=${OPTARG};;
        w) OVERWRITE="force";;
        c) CENTRAL_HUB="true";;
        \?) echo "Invalid option: -${OPTARG}" >&2
            exit 1;;
        :) echo "Option -${OPTARG} requires an argument" >&2
            exit 1;;
    esac
done

# Test for data dir
if [[ -z $BASE_DIR ]]
then
    echo "You must specify a base directory using -b"
    echo "${USAGE}"
    exit
fi

if [[ ! -d $BASE_DIR ]]
then
    echo "The base directory ${BASE_DIR} you specified using -b does not exist"
    echo "${USAGE}"
    exit
fi

# Test for analysis dir
if [[ -z $ANALYSIS_DIR ]]
then
    echo "You must specify an analysis directory using -a"
    echo $USAGE
    exit
fi

if [[ ! -d ${BASE_DIR}/${ANALYSIS_DIR} ]]
then
    echo "The analysis directory ${BASE_DIR}/${ANALYSIS_DIR} you specified using -b and -a does not exist"
    echo "${USAGE}"
    exit
fi


# test assembly output dir
if [[ -z $ASSEMBLY_OUTPUT_DIR ]]
then
    echo "You must specify the name of the assembly output dir using -o"
    echo $USAGE
    exit
fi

if [[ ! -d ${BASE_DIR}/${ANALYSIS_DIR}/${ASSEMBLY_OUTPUT_DIR} ]]
then
    echo "The assembly output directory ${BASE_DIR}/${ANALYSIS_DIR}/${ASSEMBLY_OUTPUT_DIR} you specified using -b, -a and -o does not exist"
    echo "${USAGE}"
    exit
fi

if [[ -z $FASTQS_DIR ]]
then
    FASTQS_DIR="fastqs"
fi

if [[ ! -d ${BASE_DIR}/${ANALYSIS_DIR}/${FASTQS_DIR} ]]
then
    echo "The fastqs directory ${BASE_DIR}/${ANALYSIS_DIR}/${FASTQS_DIR} you specified using -b, -a and -f does not exist"
    echo "${USAGE}"
    exit
fi

if [[ -z $OVERWRITE ]]
then
    OVERWRITE="ignore"
fi

if [[ -z $CENTRAL_HUB ]]
then
    CENTRAL_HUB="false"
fi

if [[ -z $READ_SUFFIX ]]
then
    READ_SUFFIX="_"
fi

echo "Link files. This may take a while. Please wait ......"

# function to link a file, possibly removing it first
test_file_and_link(){
    SOURCE=$1
    TARGET=$2
    FORCE=$3
    BASENAME=$(basename ${SOURCE})

    if [[ -f ${TARGET}/${BASENAME} ]]; then
        if [[ ${FORCE} == "force" ]]; then
            rm ${TARGET}/${BASENAME}
            ln -s $(readlink -f ${SOURCE}) ${TARGET}
        else
            echo "File ${TARGET}/${BASENAME} exists. Use the -w flag if you want to overwrite the file"
            fi
    else
        ln -s $(readlink -f ${SOURCE}) ${TARGET}
    fi
}


# function to link all files for a sample
link_data(){
    BASE_DIR=$1
    ANALYSIS_DIR=$2
    ASSEMBLY_OUTPUT_DIR=$3
    FASTQS_DIR=$4
    PREFIX=$5
    RESULT=$6
    SPECIES=$7
    FORCE=$8
    CENTRAL_HUB=$9
    READ_SUFFIX=${10}

    # link fastqs
    # make directories by making failure the species directory will also be made
    if [[ ! -d ${BASE_DIR}/project_species_fastqs/${SPECIES} ]]; then
        mkdir -p ${BASE_DIR}/project_species_fastqs/${SPECIES}/FAILURE
    fi
    if [[ ! -d ${BASE_DIR}/${ANALYSIS_DIR}/species_fastqs/${SPECIES} ]]; then
        mkdir -p ${BASE_DIR}/${ANALYSIS_DIR}/species_fastqs/${SPECIES}/FAILURE
    fi
    if [[ ! -d ${BASE_DIR}/../species_data/fastqs/${SPECIES} ]]; then
        mkdir -p ${BASE_DIR}/../species_data/fastqs/${SPECIES}/FAILURE
    fi

    # determine destination directories
    if [[ ${RESULT} == 'FAILURE' ]]; then
        HUB_DESTINATION_DIR=${BASE_DIR}/../species_data/fastqs/${SPECIES}/FAILURE
        PROJECT_DESTINATION_DIR=${BASE_DIR}/project_species_fastqs/${SPECIES}/FAILURE
        ANALYSIS_DESTINATION_DIR=${BASE_DIR}/${ANALYSIS_DIR}/species_fastqs/${SPECIES}/FAILURE
    else
        HUB_DESTINATION_DIR=${BASE_DIR}/../species_data/fastqs/${SPECIES}
        PROJECT_DESTINATION_DIR=${BASE_DIR}/project_species_fastqs/${SPECIES}
        ANALYSIS_DESTINATION_DIR=${BASE_DIR}/${ANALYSIS_DIR}/species_fastqs/${SPECIES}
    fi

    ##############################   Link fastqs to destination directories ##############################
    test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${FASTQS_DIR}/${PREFIX}${READ_SUFFIX}1.fastq.gz ${PROJECT_DESTINATION_DIR}/${PREFIX}${READ_SUFFIX}1.fastq.gz ${OVERWRITE}
    test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${FASTQS_DIR}/${PREFIX}${READ_SUFFIX}2.fastq.gz ${PROJECT_DESTINATION_DIR}/${PREFIX}${READ_SUFFIX}2.fastq.gz ${OVERWRITE}
    test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${FASTQS_DIR}/${PREFIX}${READ_SUFFIX}1.fastq.gz ${ANALYSIS_DESTINATION_DIR}/${PREFIX}${READ_SUFFIX}1.fastq.gz ${OVERWRITE}
    test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${FASTQS_DIR}/${PREFIX}${READ_SUFFIX}2.fastq.gz ${ANALYSIS_DESTINATION_DIR}/${PREFIX}${READ_SUFFIX}2.fastq.gz ${OVERWRITE}

    if [[ ${CENTRAL_HUB} == 'true' ]]; then
        test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${FASTQS_DIR}/${PREFIX}${READ_SUFFIX}1.fastq.gz ${HUB_DESTINATION_DIR}/${PREFIX}${READ_SUFFIX}1.fastq.gz ${OVERWRITE}
        test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${FASTQS_DIR}/${PREFIX}${READ_SUFFIX}2.fastq.gz ${HUB_DESTINATION_DIR}/${PREFIX}${READ_SUFFIX}2.fastq.gz ${OVERWRITE}
    fi

    # ---------------------- Handle Salmonella ---------------------- #
    if [ -z "${SPECIES##*enterica_serovar*}" ]; then
        if [[ ! -d ${BASE_DIR}/project_species_fastqs/salmonella_enterica ]]; then
            mkdir -p ${BASE_DIR}/project_species_fastqs/salmonella_enterica/FAILURE
        fi
        if [[ ! -d ${BASE_DIR}/${ANALYSIS_DIR}/species_fastqs/salmonella_enterica ]]; then
            mkdir -p ${BASE_DIR}/${ANALYSIS_DIR}/species_fastqs/salmonella_enterica/FAILURE
        fi
        if [[ ! -d ${BASE_DIR}/../species_data/fastqs/salmonella_enterica ]]; then
            mkdir -p ${BASE_DIR}/../species_data/fastqs/salmonella_enterica/FAILURE
        fi

        if [[ ${RESULT} == 'FAILURE' ]]; then
            HUB_DESTINATION_DIR=${BASE_DIR}/../species_data/fastqs/salmonella_enterica/FAILURE
            PROJECT_DESTINATION_DIR=${BASE_DIR}/project_species_fastqs/salmonella_enterica/FAILURE
            ANALYSIS_DESTINATION_DIR=${BASE_DIR}/${ANALYSIS_DIR}/species_fastqs/salmonella_enterica/FAILURE
        else
            HUB_DESTINATION_DIR=${BASE_DIR}/../species_data/fastqs/salmonella_enterica
            PROJECT_DESTINATION_DIR=${BASE_DIR}/project_species_fastqs/salmonella_enterica
            ANALYSIS_DESTINATION_DIR=${BASE_DIR}/${ANALYSIS_DIR}/species_fastqs/salmonella_enterica
        fi

        test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${FASTQS_DIR}/${PREFIX}${READ_SUFFIX}1.fastq.gz ${PROJECT_DESTINATION_DIR}/${PREFIX}${READ_SUFFIX}1.fastq.gz ${OVERWRITE}
        test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${FASTQS_DIR}/${PREFIX}${READ_SUFFIX}2.fastq.gz ${PROJECT_DESTINATION_DIR}/${PREFIX}${READ_SUFFIX}2.fastq.gz ${OVERWRITE}
        test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${FASTQS_DIR}/${PREFIX}${READ_SUFFIX}1.fastq.gz ${ANALYSIS_DESTINATION_DIR}/${PREFIX}${READ_SUFFIX}1.fastq.gz ${OVERWRITE}
        test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${FASTQS_DIR}/${PREFIX}${READ_SUFFIX}2.fastq.gz ${ANALYSIS_DESTINATION_DIR}/${PREFIX}${READ_SUFFIX}2.fastq.gz ${OVERWRITE}

        if [[ ${CENTRAL_HUB} == 'true' ]]; then
            test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${FASTQS_DIR}/${PREFIX}${READ_SUFFIX}1.fastq.gz ${HUB_DESTINATION_DIR}/${PREFIX}${READ_SUFFIX}1.fastq.gz ${OVERWRITE}
            test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${FASTQS_DIR}/${PREFIX}${READ_SUFFIX}2.fastq.gz ${HUB_DESTINATION_DIR}/${PREFIX}${READ_SUFFIX}2.fastq.gz ${OVERWRITE}
        fi
    fi

    ##############################   Link scaffold fastas to destination directories ##############################
    if [[ ! -d ${BASE_DIR}/project_species_fastas/${SPECIES} ]]; then
        mkdir -p ${BASE_DIR}/project_species_fastas/${SPECIES}/FAILURE
    fi
    if [[ ! -d ${BASE_DIR}/${ANALYSIS_DIR}/species_fastas/${SPECIES} ]]; then
        mkdir -p ${BASE_DIR}/${ANALYSIS_DIR}/species_fastas/${SPECIES}/FAILURE
    fi
    if [[ ! -d ${BASE_DIR}/../species_data/fastas/${SPECIES} ]]; then
        mkdir -p ${BASE_DIR}/../species_data/fastas/${SPECIES}/FAILURE
    fi

    # determine destination directories
    if [[ ${RESULT} == 'FAILURE' ]]; then
        HUB_DESTINATION_DIR=${BASE_DIR}/../species_data/fastas/${SPECIES}/FAILURE
        PROJECT_DESTINATION_DIR=${BASE_DIR}/project_species_fastas/${SPECIES}/FAILURE
        ANALYSIS_DESTINATION_DIR=${BASE_DIR}/${ANALYSIS_DIR}/species_fastas/${SPECIES}/FAILURE
    else
        HUB_DESTINATION_DIR=${BASE_DIR}/../species_data/fastas/${SPECIES}
        PROJECT_DESTINATION_DIR=${BASE_DIR}/project_species_fastas/${SPECIES}
        ANALYSIS_DESTINATION_DIR=${BASE_DIR}/${ANALYSIS_DIR}/species_fastas/${SPECIES}
    fi

    LOWER_CASE_RESULT=$(echo $RESULT | awk {'print tolower($1)'})

    test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${ASSEMBLY_OUTPUT_DIR}/assemblies/${LOWER_CASE_RESULT}/${PREFIX}*.fasta ${PROJECT_DESTINATION_DIR}/${PREFIX}.fasta ${OVERWRITE}
    test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${ASSEMBLY_OUTPUT_DIR}/assemblies/${LOWER_CASE_RESULT}/${PREFIX}*.fasta ${ANALYSIS_DESTINATION_DIR}/${PREFIX}.fasta ${OVERWRITE}
    
    if [[ ${CENTRAL_HUB} == 'true' ]]; then
        test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${ASSEMBLY_OUTPUT_DIR}/assemblies/${LOWER_CASE_RESULT}/${PREFIX}*.fasta ${HUB_DESTINATION_DIR}/${PREFIX}.fasta ${OVERWRITE}
    fi

    # ---------------------- Handle Salmonella ---------------------- #
    if [ -z "${SPECIES##*enterica_serovar*}" ]; then
        if [[ ! -d ${BASE_DIR}/project_species_fastas/salmonella_enterica ]]; then
            mkdir -p ${BASE_DIR}/project_species_fastas/salmonella_enterica/FAILURE
        fi
        if [[ ! -d ${BASE_DIR}/${ANALYSIS_DIR}/species_fastas/salmonella_enterica ]]; then
            mkdir -p ${BASE_DIR}/${ANALYSIS_DIR}/species_fastas/salmonella_enterica/FAILURE
        fi
        if [[ ! -d ${BASE_DIR}/../species_data/fastas/salmonella_enterica ]]; then
            mkdir -p ${BASE_DIR}/../species_data/fastas/salmonella_enterica/FAILURE
        fi

        if [[ ${RESULT} == 'FAILURE' ]]; then
            HUB_DESTINATION_DIR=${BASE_DIR}/../species_data/fastas/salmonella_enterica/FAILURE
            PROJECT_DESTINATION_DIR=${BASE_DIR}/project_species_fastas/salmonella_enterica/FAILURE
            ANALYSIS_DESTINATION_DIR=${BASE_DIR}/${ANALYSIS_DIR}/species_fastas/salmonella_enterica/FAILURE
        else
            HUB_DESTINATION_DIR=${BASE_DIR}/../species_data/fastas/salmonella_enterica
            PROJECT_DESTINATION_DIR=${BASE_DIR}/project_species_fastas/salmonella_enterica
            ANALYSIS_DESTINATION_DIR=${BASE_DIR}/${ANALYSIS_DIR}/species_fastas/salmonella_enterica
        fi
        test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${ASSEMBLY_OUTPUT_DIR}/assemblies/${LOWER_CASE_RESULT}/${PREFIX}*.fasta ${PROJECT_DESTINATION_DIR}/${PREFIX}.fasta ${OVERWRITE}
        test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${ASSEMBLY_OUTPUT_DIR}/assemblies/${LOWER_CASE_RESULT}/${PREFIX}*.fasta ${ANALYSIS_DESTINATION_DIR}/${PREFIX}.fasta ${OVERWRITE}

        if [[ ${CENTRAL_HUB} == 'true' ]]; then
            test_file_and_link ${BASE_DIR}/${ANALYSIS_DIR}/${ASSEMBLY_OUTPUT_DIR}/assemblies/${LOWER_CASE_RESULT}/${PREFIX}*.fasta ${HUB_DESTINATION_DIR}/${PREFIX}.fasta ${OVERWRITE}
        fi
    fi
}

# Main script
COUNTER=0
tail -n +2 $BASE_DIR/$ANALYSIS_DIR/$ASSEMBLY_OUTPUT_DIR/quality_reports/qualifyr_report.tsv |
while read LINE  || [[ -n $LINE ]]
do
    PREFIX=$(echo "$LINE" | awk -F $'\t' '{ print($1) }')
    RESULT=$(echo "$LINE" | awk -F $'\t' '{ print($2) }')
    SPECIES=$(echo "$LINE" | awk -F $'\t' '{ species = tolower($5); gsub(" ", "_", species); print(species)}')

    link_data $BASE_DIR $ANALYSIS_DIR $ASSEMBLY_OUTPUT_DIR $FASTQS_DIR $PREFIX $RESULT $SPECIES $OVERWRITE $CENTRAL_HUB $READ_SUFFIX
    COUNTER=$((COUNTER + 1))
    if [[ $(( $COUNTER % 10 )) == 0 ]]; then
        echo -ne "Linked ${COUNTER} samples"'\r'
    fi
done

if [[ ${CENTRAL_HUB} == 'true' ]]; then
    PROJECT_BASENAME=$(basename ${BASE_DIR})
    ANALYSIS_BASE_DIR=$(basename ${ANALYSIS_DIR})
    echo "Commit these changes to git"
    echo "cd ${BASE_DIR}/../species_data && git add . && git commit -m 'adding ${PROJECT_BASENAME} batch ${ANALYSIS_BASE_DIR} on $(date '+%Y-%m-%d')' && git push && cd .."
fi
