#!/bin/bash
#BSUB -o /lustre/scratch118/infgen/team212/cgps/ghru/retrospective_1/<PROJECT>/batches/<BATCH>/cluster_logs/%J.o
#BSUB -e /lustre/scratch118/infgen/team212/cgps/ghru/retrospective_1/<PROJECT>/batches/<BATCH>/cluster_logs/%J.e
#BSUB -R "select[mem>8000] rusage[mem=8000]"
#BSUB -M 8000
#BSUB -q long
#BSUB -n 4

export HTTP_PROXY='http://wwwcache.sanger.ac.uk:3128'
export HTTPS_PROXY='http://wwwcache.sanger.ac.uk:3128'
export NXF_ANSI_LOG=false
export NXF_OPTS="-Xms8G -Xmx8G -Dnxf.pool.maxThreads=2000"

SPECIES=<SPECIES>
DATE=<DATE>

NEXFLOW_WORKFLOWS_DIR='/lustre/scratch118/infgen/team212/au3/nextflow_workflows'
DATA_DIR='/lustre/scratch118/infgen/team212/cgps/ghru/retrospective_2'

/lustre/scratch118/infgen/team212/cgps/ghru/bin/nextflow run \
${NEXFLOW_WORKFLOWS_DIR}/roary/roary.nf \
--input_dir ${DATA_DIR}/species_data/fastas/<SPECIES> \
--fasta_pattern '*.fasta' \
--output_dir  ${DATA_DIR}/roary_output/${SPECIES}/${DATE} \
--max_clusters 100000 \
--tree \
-w ${DATA_DIR}/roary_output/${SPECIES}/${DATE}/work \
-profile sanger \
-resume

# clean up on exit 0
status=$?
## take some decision ##
if [[ $status -eq 0 ]]; then
  rm -r ${DATA_DIR}/roary_output/${SPECIES}/${DATE}/work
fi
