#!/bin/bash
#BSUB -o /lustre/scratch118/infgen/team212/cgps/ghru/retrospective_2/cluster_logs/%J.o
#BSUB -e /lustre/scratch118/infgen/team212/cgps/ghru/retrospective_2/cluster_logs/%J.e
#BSUB -R "select[mem>8000] rusage[mem=8000]"
#BSUB -M 8000
#BSUB -q long
#BSUB -n 4

export HTTP_PROXY='http://wwwcache.sanger.ac.uk:3128'
export HTTPS_PROXY='http://wwwcache.sanger.ac.uk:3128'
export NXF_ANSI_LOG=false
export NXF_OPTS="-Xms8G -Xmx8G -Dnxf.pool.maxThreads=2000"

NEXFLOW_WORKFLOWS_DIR='/lustre/scratch118/infgen/team212/au3/nextflow_workflows'
DATA_DIR='/lustre/scratch118/infgen/team212/cgps/ghru/retrospective_2'

SPECIES=<SPECIES>
VALID_SPECIES=false
for TEST in achromobacter_spp. acinetobacter_baumannii aeromonas_spp. anaplasma_phagocytophilum arcobacter_spp. aspergillus_fumigatus bacillus_cereus bacillus_licheniformis bacillus_subtilis bartonella_bacilliformis bartonella_henselae bartonella_washoensis bordetella_spp. borrelia_spp. brachyspira_hampsonii brachyspira_hyodysenteriae brachyspira_intermedia brachyspira_pilosicoli brachyspira_spp. brucella_spp. burkholderia_cepacia_complex burkholderia_pseudomallei campylobacter_concisus_curvus campylobacter_fetus campylobacter_helveticus campylobacter_hyointestinalis campylobacter_insulaenigrae campylobacter_jejuni campylobacter_lanienae campylobacter_lari campylobacter_sputorum campylobacter_upsaliensis candida_albicans candida_glabrata candida_krusei candida_tropicalis candidatus_liberibacter_solanacearum carnobacterium_maltaromaticum chlamydiales_spp. citrobacter_freundii clonorchis_sinensis clostridioides_difficile clostridium_botulinum clostridium_septicum corynebacterium_diphtheriae cronobacter_spp. dichelobacter_nodosus edwardsiella_spp. enterobacter_cloacae enterococcus_faecalis enterococcus_faecium escherichia_coli flavobacterium_psychrophilum gallibacterium_anatis haemophilus_influenzae haemophilus_parasuis helicobacter_cinaedi helicobacter_pylori helicobacter_suis kingella_kingae klebsiella_aerogenes klebsiella_oxytoca klebsiella_pneumoniae kudoa_septempunctata lactobacillus_salivarius leptospira_spp. listeria_monocytogenes macrococcus_canis macrococcus_caseolyticus mannheimia_haemolytica melissococcus_plutonius moraxella_catarrhalis mycobacteria_spp. mycobacterium_abscessus mycobacterium_massiliense mycoplasma_agalactiae mycoplasma_bovis mycoplasma_flocculare mycoplasma_hominis mycoplasma_hyopneumoniae mycoplasma_hyorhinis mycoplasma_iowae mycoplasma_pneumoniae mycoplasma_synoviae neisseria_spp. orientia_tsutsugamushi ornithobacterium_rhinotracheale paenibacillus_larvae pasteurella_multocida pediococcus_pentosaceus photobacterium_damselae piscirickettsia_salmonis porphyromonas_gingivalis propionibacterium_acnes pseudomonas_aeruginosa pseudomonas_fluorescens pseudomonas_putida rhodococcus_spp. riemerella_anatipestifer salmonella_enterica saprolegnia_parasitica sinorhizobium_spp. staphylococcus_aureus staphylococcus_epidermidis staphylococcus_haemolyticus staphylococcus_hominis staphylococcus_lugdunensis staphylococcus_pseudintermedius stenotrophomonas_maltophilia streptococcus_agalactiae streptococcus_bovis_equinus_complex streptococcus_canis streptococcus_dysgalactiae_equisimilis streptococcus_gallolyticus streptococcus_oralis streptococcus_pneumoniae streptococcus_pyogenes streptococcus_suis streptococcus_thermophilus streptococcus_uberis streptococcus_zooepidemicus streptomyces_spp taylorella_spp. tenacibaculum_spp. treponema_pallidum trichomonas_vaginalis ureaplasma_spp. vibrio_cholerae vibrio_parahaemolyticus vibrio_spp. vibrio_tapetis vibrio_vulnificus wolbachia xylella_fastidiosa yersinia_pseudotuberculosis yersinia_ruckeri yersinia_spp. 
do
    if [[ ${TEST} == *_spp. ]]
    then
        TEST_GENUS=${TEST%*_spp.}
        SPECIES_GENUS=${SPECIES%*_*}
        if [[ $TEST_GENUS == $SPECIES_GENUS ]]
        then
            VALID_SPECIES=$TEST
        fi
    else   
        if [[ $SPECIES == $TEST ]]
        then
            VALID_SPECIES=$TEST
        fi
    fi
done

if [[ $VALID_SPECIES == "false" ]]
then
    cat << EOM
$SPECIES is not valid. Supported MLST schemes are:
achromobacter_spp.
acinetobacter_baumannii
aeromonas_spp.
anaplasma_phagocytophilum
arcobacter_spp.
aspergillus_fumigatus
bacillus_cereus
bacillus_licheniformis
bacillus_subtilis
bartonella_bacilliformis
bartonella_henselae
bartonella_washoensis
bordetella_spp.
borrelia_spp.
brachyspira_hampsonii
brachyspira_hyodysenteriae
brachyspira_intermedia
brachyspira_pilosicoli
brachyspira_spp.
brucella_spp.
burkholderia_cepacia_complex
burkholderia_pseudomallei
campylobacter_concisus_curvus
campylobacter_fetus
campylobacter_helveticus
campylobacter_hyointestinalis
campylobacter_insulaenigrae
campylobacter_jejuni
campylobacter_lanienae
campylobacter_lari
campylobacter_sputorum
campylobacter_upsaliensis
candida_albicans
candida_glabrata
candida_krusei
candida_tropicalis
candidatus_liberibacter_solanacearum
carnobacterium_maltaromaticum
chlamydiales_spp.
citrobacter_freundii
clonorchis_sinensis
clostridioides_difficile
clostridium_botulinum
clostridium_septicum
corynebacterium_diphtheriae
cronobacter_spp.
dichelobacter_nodosus
edwardsiella_spp.
enterobacter_cloacae
enterococcus_faecalis
enterococcus_faecium
escherichia_coli
flavobacterium_psychrophilum
gallibacterium_anatis
haemophilus_influenzae
haemophilus_parasuis
helicobacter_cinaedi
helicobacter_pylori
helicobacter_suis
kingella_kingae
klebsiella_aerogenes
klebsiella_oxytoca
klebsiella_pneumoniae
kudoa_septempunctata
lactobacillus_salivarius
leptospira_spp.
listeria_monocytogenes
macrococcus_canis
macrococcus_caseolyticus
mannheimia_haemolytica
melissococcus_plutonius
moraxella_catarrhalis
mycobacteria_spp.
mycobacterium_abscessus
mycobacterium_massiliense
mycoplasma_agalactiae
mycoplasma_bovis
mycoplasma_flocculare
mycoplasma_hominis
mycoplasma_hyopneumoniae
mycoplasma_hyorhinis
mycoplasma_iowae
mycoplasma_pneumoniae
mycoplasma_synoviae
neisseria_spp.
orientia_tsutsugamushi
ornithobacterium_rhinotracheale
paenibacillus_larvae
pasteurella_multocida
pediococcus_pentosaceus
photobacterium_damselae
piscirickettsia_salmonis
porphyromonas_gingivalis
propionibacterium_acnes
pseudomonas_aeruginosa
pseudomonas_fluorescens
pseudomonas_putida
rhodococcus_spp.
riemerella_anatipestifer
salmonella_enterica
saprolegnia_parasitica
sinorhizobium_spp.
staphylococcus_aureus
staphylococcus_epidermidis
staphylococcus_haemolyticus
staphylococcus_hominis
staphylococcus_lugdunensis
staphylococcus_pseudintermedius
stenotrophomonas_maltophilia
streptococcus_agalactiae
streptococcus_bovis_equinus_complex
streptococcus_canis
streptococcus_dysgalactiae_equisimilis
streptococcus_gallolyticus
streptococcus_oralis
streptococcus_pneumoniae
streptococcus_pyogenes
streptococcus_suis
streptococcus_thermophilus
streptococcus_uberis
streptococcus_zooepidemicus
streptomyces_spp
taylorella_spp.
tenacibaculum_spp.
treponema_pallidum
trichomonas_vaginalis
ureaplasma_spp.
vibrio_cholerae
vibrio_parahaemolyticus
vibrio_spp.
vibrio_tapetis
vibrio_vulnificus
wolbachia
xylella_fastidiosa
yersinia_pseudotuberculosis
yersinia_ruckeri
yersinia_spp. 
EOM
else
    MLST_SPECIES=$(echo ${VALID_SPECIES} | sed -e 's/^\(.\)/\U\1/' | sed -e 's/_/ /g')
    /lustre/scratch118/infgen/team212/cgps/ghru/bin/nextflow run \
    ${NEXFLOW_WORKFLOWS_DIR}/mlst-1.0/main.nf \
    --input_dir ${DATA_DIR}/species_data/fastqs/<SPECIES> \
    --fastq_pattern '*{R,_}{1,2}.f*q.gz' \
    --output_dir ${DATA_DIR}/mlst_output/<SPECIES>/<DATE> \
    --read_polishing_depth_cutoff 100 \
    --mlst_species "${MLST_SPECIES}" \
    -w ${DATA_DIR}/mlst_output/<SPECIES>/<DATE>/work \
    -profile sanger -qs 1000 -resume

    # clean up on exit 0
    status=$?
    ## take some decision ##
    if [[ $status -eq 0 ]]; then
        rm -r ${DATA_DIR}/mlst_output/<SPECIES>/<DATE>/work
    fi
fi
