#!/bin/bash
#BSUB -o /lustre/scratch118/infgen/team212/cgps/ghru/retrospective_2/<PROJECT>/batches/<BATCH>/cluster_logs/%J.o
#BSUB -e /lustre/scratch118/infgen/team212/cgps/ghru/retrospective_2/<PROJECT>/batches/<BATCH>/cluster_logs/%J.e
#BSUB -R "select[mem>8000] rusage[mem=8000]"
#BSUB -M 8000
#BSUB -q long
#BSUB -n 4

export HTTP_PROXY='http://wwwcache.sanger.ac.uk:3128'
export HTTPS_PROXY='http://wwwcache.sanger.ac.uk:3128'
export NXF_ANSI_LOG=false
export NXF_OPTS="-Xms8G -Xmx8G -Dnxf.pool.maxThreads=2000"

NEXTFLOW_WORKFLOWS_DIR='/lustre/scratch118/infgen/team212/au3/nextflow_workflows'
DATA_DIR='/lustre/scratch118/infgen/team212/cgps/ghru/retrospective_2/<PROJECT>/batches/<BATCH>'

/lustre/scratch118/infgen/team212/cgps/ghru/bin/nextflow run \
${NEXTFLOW_WORKFLOWS_DIR}/assembly-1.5.5/assembly.nf \
--adapter_file ${NEXTFLOW_WORKFLOWS_DIR}/assembly-1.5.5/adapters.fas \
--qc_conditions ${NEXTFLOW_WORKFLOWS_DIR}/assembly-1.5.5/qc_conditions_nextera_relaxed.yml \
--input_dir ${DATA_DIR}/fastqs \
--fastq_pattern '*{R,_}{1,2}.f*q.gz' \
--output_dir ${DATA_DIR}/assembly_output \
--confindr_db_path /lustre/scratch118/infgen/team212/au3/software/confindr/confindr_database \
--depth_cutoff 100 \
--careful \
-w ${DATA_DIR}/work \
-profile sanger -qs 1000 -resume

# clean up on exit 0
status=$?
## take some decision ##
if [[ $status -eq 0 ]]; then
  rm -r ${DATA_DIR}/work
fi
