#!/bin/bash
#BSUB -o /lustre/scratch118/infgen/team212/cgps/ghru/retrospective_2/cluster_logs/%J.o
#BSUB -e /lustre/scratch118/infgen/team212/cgps/ghru/retrospective_2/cluster_logs/%J.e
#BSUB -R "select[mem>8000] rusage[mem=8000]"
#BSUB -M 8000
#BSUB -q long
#BSUB -n 4

export HTTP_PROXY='http://wwwcache.sanger.ac.uk:3128'
export HTTPS_PROXY='http://wwwcache.sanger.ac.uk:3128'
export NXF_ANSI_LOG=false
export NXF_OPTS="-Xms8G -Xmx8G -Dnxf.pool.maxThreads=2000"

NEXFLOW_WORKFLOWS_DIR='/lustre/scratch118/infgen/team212/au3/nextflow_workflows'
DATA_DIR='/lustre/scratch118/infgen/team212/cgps/ghru/retrospective_2'

SPECIES=<SPECIES>
ARIBA_POINTFINDER_SPECIES=""
for TEST in campylobacter enterococcus_faecalis enterococcus_faecium escherichia_coli helicobacter_pylori klebsiella mycobacterium_tuberculosis neisseria_gonorrhoeae salmonella staphylococcus_aureus
do
    if [[ $SPECIES == $TEST* ]]
    then
        ARIBA_POINTFINDER_SPECIES=$TEST
    fi
done

if [[ $ARIBA_POINTFINDER_SPECIES != "" ]]
then
    /lustre/scratch118/infgen/team212/cgps/ghru/bin/nextflow run \
    ${NEXFLOW_WORKFLOWS_DIR}/amr-prediction-1.1/main.nf \
    --input_dir ${DATA_DIR}/species_data/fastqs/<SPECIES> \
    --fastq_pattern '*{R,_}{1,2}.f*q.gz' \
    --output_dir ${DATA_DIR}/amr_prediction_output/<SPECIES>/<DATE> \
    --read_polishing_adapter_file ${NEXFLOW_WORKFLOWS_DIR}/amr-prediction-1.1/adapters.fas \
    --read_polishing_depth_cutoff 100 \
    --species ${ARIBA_POINTFINDER_SPECIES} \
    -w ${DATA_DIR}/amr_prediction_output/<SPECIES>/<DATE>/work \
    -profile sanger -qs 1000 -resume
else
    /lustre/scratch118/infgen/team212/cgps/ghru/bin/nextflow run \
    ${NEXFLOW_WORKFLOWS_DIR}/amr-prediction-1.1/main.nf \
    --input_dir ${DATA_DIR}/species_data/fastqs/<SPECIES> \
    --fastq_pattern '*{R,_}{1,2}.f*q.gz' \
    --output_dir ${DATA_DIR}/amr_prediction_output/<SPECIES>/<DATE> \
    --read_polishing_adapter_file ${NEXFLOW_WORKFLOWS_DIR}/amr-prediction-1.1/adapters.fas \
    --read_polishing_depth_cutoff 100 \
    -w ${DATA_DIR}/amr_prediction_output/<SPECIES>/<DATE>/work \
    -profile sanger -qs 1000 -resume
 fi
 # clean up on exit 0
 status=$?
 ## take some decision ##
 if [[ $status -eq 0 ]]; then
     rm -r ${DATA_DIR}/amr_prediction_output/<SPECIES>/<DATE>/work
 fi
