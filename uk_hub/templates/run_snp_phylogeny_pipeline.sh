#!/bin/bash
#BSUB -o /lustre/scratch118/infgen/team212/cgps/ghru/retrospective_2/cluster_logs/%J.o
#BSUB -e /lustre/scratch118/infgen/team212/cgps/ghru/retrospective_2/cluster_logs/%J.e
#BSUB -R "select[mem>8000] rusage[mem=8000]"
#BSUB -M 8000
#BSUB -q long
#BSUB -n 4

export HTTP_PROXY='http://wwwcache.sanger.ac.uk:3128'
export HTTPS_PROXY='http://wwwcache.sanger.ac.uk:3128'
export NXF_ANSI_LOG=false
export NXF_OPTS="-Xms8G -Xmx8G -Dnxf.pool.maxThreads=2000"


SPECIES=<SPECIES>
DATE=<DATE>
RECOMBINATION=<RECOMBINATION>
REFERENCE=<REFERENCE>

NEXFLOW_WORKFLOWS_DIR='/lustre/scratch118/infgen/team212/au3/nextflow_workflows'
DATA_DIR='/lustre/scratch118/infgen/team212/cgps/ghru/retrospective_2'

/lustre/scratch118/infgen/team212/cgps/ghru/bin/nextflow run \
${NEXFLOW_WORKFLOWS_DIR}/snp_phylogeny-1.2.2/snp_phylogeny.nf \
--input_dir ${DATA_DIR}/species_data/fastqs/${SPECIES} \
--fastq_pattern '*_{1,2}.fastq.gz' \
--output_dir ${DATA_DIR}/snp_phylogeny_output/${SPECIES}/${DATE} \
--reference ${DATA_DIR}/references/${REFERENCE} \
${RECOMBINATION} \
--tree \
--simple_output \
-w ${DATA_DIR}/snp_phylogeny_output/${SPECIES}/${DATE}/work \
-profile sanger -qs 1000 -resume

# clean up on exit 0
status=$?
## take some decision ##
if [[ $status -eq 0 ]]; then
  rm -r ${DATA_DIR}/snp_phylogeny_output/${SPECIES}/${DATE}/work
fi
