#!/usr/bin/env bash
USAGE="get_fastqs_and_info.sh -b <BASE DATA DIR> -i <PROJECT_ID>"
while getopts ":b:i:dv" option
do
    case "${option}" in
        b) BASE_DIR=${OPTARG};;
        i) PROJECT_ID=${OPTARG};;
        d) DRY_RUN="true";;
        v) VERBOSE="true";;
        \?) echo "Invalid option: -${OPTARG}" >&2
            exit 1;;
        :) echo "Option -${OPTARG} requires an argument" >&2
            exit 1;;
    esac
done

# Test for data dir
if [[ -z $BASE_DIR ]]
then
    echo "You must specify a base directory using -b"
    echo "${USAGE}"
    exit
fi

if [[ ! -d ${BASE_DIR}/all_fastqs_to_date ]]
then
    echo "The base directory ${BASE_DIR} you specified using -b does not exist or it does not contain the sub-directory all_fastqs_to_date"
    echo "${USAGE}"
    exit
fi
# Test for project id
if [[ -z ${PROJECT_ID} ]]
then
    echo "You must specify a project_id -i"
    echo "${USAGE}"
    exit
fi

# Set dry run or not
if [[ -z $DRY_RUN ]]
then
    DRY_RUN="false"
    FILE_SUFFIX="txt"
else
    FILE_SUFFIX="DRY_RUN.txt"
fi

# Set verbose or not
if [[ -z $VERBOSE ]]
then
    VERBOSE="false"
fi

if [[ $DRY_RUN == "false" ]]
then
    echo "Getting fastqs ...... (Please wait)"
    pf data --type study --id ${PROJECT_ID} --filetype fastq --symlink ${BASE_DIR}/all_fastqs_to_date/temp_fastqs > /dev/null
fi

echo "Getting info ...... (Please wait)"
pf info --type study --id ${PROJECT_ID} > ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.${FILE_SUFFIX}

# remove ignored lanes or samples
if [[ -f  ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.${FILE_SUFFIX} ]]
then
    rm  ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.${FILE_SUFFIX}
fi

while read LINE
do
    IGNORED=0
    while read IGNORED_LANE_OR_SAMPLE
    do
        # is it a sample in the formant XXXXX_X#XX
        if echo $IGNORED_LANE_OR_SAMPLE |  grep -q '^[^#][0-9]*_[0-9]#[0-9]*'
        then
            # check if ignored sample
            if echo $LINE | grep -Eqw ${IGNORED_LANE_OR_SAMPLE}
            then
                IGNORED=1
            fi
        elif echo $IGNORED_LANE_OR_SAMPLE |  grep -q '^[^#][0-9]*_[0-9]*$'
        then
            # check if ignored lane
            if [[ $LINE == ${IGNORED_LANE_OR_SAMPLE}* ]]
            then
                IGNORED=1
            fi
        fi
    done < ${BASE_DIR}/all_fastqs_to_date/ignored_lanes_and_samples.txt
    if [[ $IGNORED -eq 0 ]]
    then
        echo "$LINE" >> ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.${FILE_SUFFIX}
    else
        if [[ $VERBOSE == "true" ]]
        then
            echo "Ignoring $(echo $LINE | awk '{print $1}')"
        fi
    fi
done < ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.${FILE_SUFFIX}


# Get supplier names
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
${SCRIPT_DIR}/get_supplier_names.sh -b ${BASE_DIR} -s ${FILE_SUFFIX} -q

# Replace blank (NA) supplier names with value from DB
if [[ -f ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.temp.txt ]]
then
    rm ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.temp.txt
fi

while read LINE
do
    SUPPLIER_NAME=$(echo $LINE | awk '{print $3}')
    LANE_ID=$(echo $LINE | awk '{print $1}')
    if [[ $SUPPLIER_NAME == "NA" ]]
    then
        # find supplier name
        NEW_SUPPLIER_NAME=$(grep -w ${LANE_ID} ${BASE_DIR}/all_fastqs_to_date/supplier_names.txt | awk '{print $3}')
        LINE=$(echo $LINE | awk -v NEW_SUPPLIER_NAME="${NEW_SUPPLIER_NAME}" '{OFS="\t"; $3 = NEW_SUPPLIER_NAME; print $0}')
    else
        LINE=$(echo $LINE | tr [:blank:] \\t)
    fi
    echo "$LINE" >> ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.temp.txt
done < ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.${FILE_SUFFIX}
mv ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.temp.txt ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.${FILE_SUFFIX}

# Check for samples where GHRU ID = NA. This value should be 0
NUM_SAMPLES_WITH_NA=$(awk '$3 == "NA" {print $0}'  ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.${FILE_SUFFIX} | wc -l)
SAMPLES_WITH_NA=$(awk '$3 == "NA" {print $1 "=>" $3}'  ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.${FILE_SUFFIX})
for SAMPLE in ${SAMPLES_WITH_NA}
do
    echo "WARNING: GHRU ID = NA: ${SAMPLE}"
done
if [[ ${NUM_SAMPLES_WITH_NA} -gt 0 ]]
then
    echo "WARNING: THERE ARE ${NUM_SAMPLES_WITH_NA} SAMPLES WHERE GHRU ID == 'NA'"
fi

# Check for duplicates.This value should be 0
NUM_DUPLICATE_SAMPLES=$(sort -k3 ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.${FILE_SUFFIX} | uniq -f2 -d | wc -l)
NUM_DUPLICATES=$(sort -k3 ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.${FILE_SUFFIX} | uniq -f2 -D | wc -l)
DUPLICATE_SAMPLES=$(sort -k3 ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.${FILE_SUFFIX} | uniq -f2 -D | awk '{print $1 "=>" $3}')
for SAMPLE in ${DUPLICATE_SAMPLES}
do
    echo "WARNING, DUPLICATE SAMPLE: ${SAMPLE}"
done
if [[ ${NUM_DUPLICATE_SAMPLES} -gt 0 ]]
then
    echo "WARNING: THERE ARE ${NUM_DUPLICATE_SAMPLES} DUPLICATE SAMPLES (${NUM_DUPLICATES} duplicates)"
fi

if [[ ${NUM_SAMPLES_WITH_NA} -gt 0 || ${NUM_DUPLICATE_SAMPLES} -gt 0 ]]
then
    echo "Please fix NA or Duplicate problems before continuing"
    NUM_SAMPLES=$(cat ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.${FILE_SUFFIX} | tail -n+2 | wc -l)
    echo "The project ${PROJECT_ID} contains ${NUM_SAMPLES} samples on iRODS (including duplicates)"
    NUM_SAMPLE_FASTQS_PROCESSED=$(wc -l < ${BASE_DIR}/all_fastqs_to_date/processed_fastqs.txt)
    echo "The number of samples processed in previous batches is $((${NUM_SAMPLE_FASTQS_PROCESSED}/2))"
    if [[ $DRY_RUN == "false" ]]
    then
	rm -r ${BASE_DIR}/all_fastqs_to_date/temp_fastqs
    fi
    exit
fi

# Move fastqs
if [[ $DRY_RUN == "false" ]]
then
    FASTQS_PROCESSED=0
    TOTAL_FASTQS=$(ls ${BASE_DIR}/all_fastqs_to_date/temp_fastqs/*.fastq.gz | wc -l)
    for FILE in ${BASE_DIR}/all_fastqs_to_date/temp_fastqs/*.fastq.gz
    do
        FASTQS_PROCESSED=$[FASTQS_PROCESSED+1]
        if [ $(echo "$FASTQS_PROCESSED % 100" | bc) -eq 0 ]
        then
            echo "Processed ${FASTQS_PROCESSED} fastqs of ${TOTAL_FASTQS}"
        fi
        FASTQ_FILE_SUFFIX=$(basename ${FILE})
        LANE_ID=${FASTQ_FILE_SUFFIX%*_?.fastq.gz}
        IGNORED=0
        while read IGNORED_LANE_OR_SAMPLE
        do
            # is it a sample in the formant XXXXX_X#XX
            if echo $IGNORED_LANE_OR_SAMPLE |  grep -q '^[^#][0-9]*_[0-9]#[0-9]*'
            then
                # check if ignored sample
                if echo $LANE_ID | grep -Eqw ${IGNORED_LANE_OR_SAMPLE}
                then
                    IGNORED=1
                fi
            elif echo $IGNORED_LANE_OR_SAMPLE |  grep -q '^[^#][0-9]*_[0-9]*$'
            then
                # check if ignored lane
                if [[ $LANE_ID == ${IGNORED_LANE_OR_SAMPLE}* ]]
                then
                    IGNORED=1
                fi
            fi
        done < ${BASE_DIR}/all_fastqs_to_date/ignored_lanes_and_samples.txt
        if [[ IGNORED  -eq 1 ]]
        then
            rm $FILE
        fi
    done

    for FILE in ${BASE_DIR}/all_fastqs_to_date/temp_fastqs/*.fastq.gz
    do
        FASTQ_FILE_SUFFIX=$(basename ${FILE})
        if [[ ! -f ${BASE_DIR}/all_fastqs_to_date/fastqs/${FASTQ_FILE_SUFFIX} ]]
        then
            mv ${FILE} ${BASE_DIR}/all_fastqs_to_date/fastqs/${FASTQ_FILE_SUFFIX}
        fi
    done
    rm -r ${BASE_DIR}/all_fastqs_to_date/temp_fastqs
fi


NUM_SAMPLES=$(cat ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.${FILE_SUFFIX} | tail -n+2 | wc -l)
if [[ $DRY_RUN == "false" ]]
then
    NUM_SAMPLES_IN_FASTQS=$(ls ${BASE_DIR}/all_fastqs_to_date/fastqs/*_1.fastq.gz | wc -l)
    if [[ $NUM_SAMPLES == $NUM_SAMPLES_IN_FASTQS ]]
    then
        echo "The project ${PROJECT_ID} contains ${NUM_SAMPLES} samples"
    else
        echo "Number of samples in samples.lane_info.ignored_lanes_and_samples_removed.txt (${NUM_SAMPLES}) does not match number of samples in ${BASE_DIR}/all_fastqs_to_date/fastqs (${NUM_SAMPLES_IN_FASTQS}). Please investigate!!"
        echo "The differences observed in 'pf info' vs 'fastqs from pf data' are:"

        diff -c1 <(cat ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.${FILE_SUFFIX} | awk {'print $1'} | tail -n +2 | sort ) <(for s in  ${BASE_DIR}/all_fastqs_to_date/fastqs/*_1.fastq.gz; do s_base=$(basename $s); s_id=${s_base%*_1.fastq.gz}; echo $s_id; done | sort)  | tail -n +4
    fi
else
    echo "The project ${PROJECT_ID} contains ${NUM_SAMPLES} samples on iRODS"
    NUM_SAMPLE_FASTQS_PROCESSED=$(wc -l < ${BASE_DIR}/all_fastqs_to_date/processed_fastqs.txt)
    echo "The number of samples processed in previous batches is $((${NUM_SAMPLE_FASTQS_PROCESSED}/2))"
    if [[ $VERBOSE == "true" ]]
    then
        echo "Differing samples are:"
        diff -c1 <(cat ${BASE_DIR}/all_fastqs_to_date/samples.lane_info.ignored_lanes_and_samples_removed.${FILE_SUFFIX} | awk {'print $3'} | tail -n +2 | sort ) <(cat ${BASE_DIR}/all_fastqs_to_date/processed_fastqs.txt | grep _1.fastq.gz | sed -e 's/_1.fastq.gz//' | sort)
    fi
fi
